import runProgram from './part1';

interface Inputs {
  noun: number;
  verb: number;
}
const output = 19690720;

const getNounAndverb = (input: string): Inputs => {
  // Pair of inputs
  const nounAndVerbInputs: Inputs = { noun: 0, verb: 0 };
  // Between 0 and 99, inclusive
  for (let i = 0; i < 100; i++) {
    for (let j = 0; j < 100; j++) {
      if (runProgram(input, i, j) === output) {
        return { ...nounAndVerbInputs, noun: i, verb: j };
      }
    }
  }

  return nounAndVerbInputs;
};

export default getNounAndverb;

enum Opcode {
  add = 1,
  multiply = 2,
  halt = 99,
}

const runProgram = (input: string, value1: number, value2: number): number => {
  const intcode = input.split(',').map((strNum) => Number(strNum));
  // Before running the program, replace position 1 and 2

  intcode[1] = value1;
  intcode[2] = value2;
  for (let i = 0; i < intcode.length; i += 4) {
    const integers = intcode.slice(i, i + 4);

    if (integers[0] === Opcode.add) {
      intcode[integers[3]] = intcode[integers[1]] + intcode[integers[2]];
    } else if (integers[0] === Opcode.multiply) {
      intcode[integers[3]] = intcode[integers[1]] * intcode[integers[2]];
    } else if (integers[0] === Opcode.halt) {
      break;
    } else {
      console.error('Something went wrong');
    }
  }
  // Value left at position 0
  return intcode[0];
};

export default runProgram;

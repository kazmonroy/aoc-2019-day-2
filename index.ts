import runProgram from './parts/part1';
import getNounAndverb from './parts/part2';
const path = './input.txt';
const file = Bun.file(path);

const input = await file.text();

// Answer Part 1
const valueAtPosition0 = runProgram(input, 12, 2);

// Answer Part 2
const { noun, verb } = getNounAndverb(input);
const result = 100 * noun + verb;

const init = () => {
  console.log({ answerPart1: valueAtPosition0, answerPart2: result });
};

init();
